// Code generated by go-swagger; DO NOT EDIT.

package admin

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	"fmt"

	"github.com/go-openapi/runtime"

	strfmt "github.com/go-openapi/strfmt"
)

// New creates a new admin API client.
func New(transport runtime.ClientTransport, formats strfmt.Registry) *Client {
	return &Client{transport: transport, formats: formats}
}

/*
Client for admin API
*/
type Client struct {
	transport runtime.ClientTransport
	formats   strfmt.Registry
}

/*
AdminCreateOrg creates an organization
*/
func (a *Client) AdminCreateOrg(params *AdminCreateOrgParams, authInfo runtime.ClientAuthInfoWriter) (*AdminCreateOrgCreated, error) {
	// TODO: Validate the params before sending
	if params == nil {
		params = NewAdminCreateOrgParams()
	}

	result, err := a.transport.Submit(&runtime.ClientOperation{
		ID:                 "adminCreateOrg",
		Method:             "POST",
		PathPattern:        "/admin/users/{username}/orgs",
		ProducesMediaTypes: []string{"application/json"},
		ConsumesMediaTypes: []string{"application/json"},
		Schemes:            []string{"http", "https"},
		Params:             params,
		Reader:             &AdminCreateOrgReader{formats: a.formats},
		AuthInfo:           authInfo,
		Context:            params.Context,
		Client:             params.HTTPClient,
	})
	if err != nil {
		return nil, err
	}
	success, ok := result.(*AdminCreateOrgCreated)
	if ok {
		return success, nil
	}
	// unexpected success response
	// safeguard: normally, absent a default response, unknown success responses return an error above: so this is a codegen issue
	msg := fmt.Sprintf("unexpected success response for adminCreateOrg: API contract not enforced by server. Client expected to get an error, but got: %T", result)
	panic(msg)
}

/*
AdminCreatePublicKey adds a public key on behalf of a user
*/
func (a *Client) AdminCreatePublicKey(params *AdminCreatePublicKeyParams, authInfo runtime.ClientAuthInfoWriter) (*AdminCreatePublicKeyCreated, error) {
	// TODO: Validate the params before sending
	if params == nil {
		params = NewAdminCreatePublicKeyParams()
	}

	result, err := a.transport.Submit(&runtime.ClientOperation{
		ID:                 "adminCreatePublicKey",
		Method:             "POST",
		PathPattern:        "/admin/users/{username}/keys",
		ProducesMediaTypes: []string{"application/json"},
		ConsumesMediaTypes: []string{"application/json"},
		Schemes:            []string{"http", "https"},
		Params:             params,
		Reader:             &AdminCreatePublicKeyReader{formats: a.formats},
		AuthInfo:           authInfo,
		Context:            params.Context,
		Client:             params.HTTPClient,
	})
	if err != nil {
		return nil, err
	}
	success, ok := result.(*AdminCreatePublicKeyCreated)
	if ok {
		return success, nil
	}
	// unexpected success response
	// safeguard: normally, absent a default response, unknown success responses return an error above: so this is a codegen issue
	msg := fmt.Sprintf("unexpected success response for adminCreatePublicKey: API contract not enforced by server. Client expected to get an error, but got: %T", result)
	panic(msg)
}

/*
AdminCreateRepo creates a repository on behalf a user
*/
func (a *Client) AdminCreateRepo(params *AdminCreateRepoParams, authInfo runtime.ClientAuthInfoWriter) (*AdminCreateRepoCreated, error) {
	// TODO: Validate the params before sending
	if params == nil {
		params = NewAdminCreateRepoParams()
	}

	result, err := a.transport.Submit(&runtime.ClientOperation{
		ID:                 "adminCreateRepo",
		Method:             "POST",
		PathPattern:        "/admin/users/{username}/repos",
		ProducesMediaTypes: []string{"application/json"},
		ConsumesMediaTypes: []string{"application/json"},
		Schemes:            []string{"http", "https"},
		Params:             params,
		Reader:             &AdminCreateRepoReader{formats: a.formats},
		AuthInfo:           authInfo,
		Context:            params.Context,
		Client:             params.HTTPClient,
	})
	if err != nil {
		return nil, err
	}
	success, ok := result.(*AdminCreateRepoCreated)
	if ok {
		return success, nil
	}
	// unexpected success response
	// safeguard: normally, absent a default response, unknown success responses return an error above: so this is a codegen issue
	msg := fmt.Sprintf("unexpected success response for adminCreateRepo: API contract not enforced by server. Client expected to get an error, but got: %T", result)
	panic(msg)
}

/*
AdminCreateUser creates a user
*/
func (a *Client) AdminCreateUser(params *AdminCreateUserParams, authInfo runtime.ClientAuthInfoWriter) (*AdminCreateUserCreated, error) {
	// TODO: Validate the params before sending
	if params == nil {
		params = NewAdminCreateUserParams()
	}

	result, err := a.transport.Submit(&runtime.ClientOperation{
		ID:                 "adminCreateUser",
		Method:             "POST",
		PathPattern:        "/admin/users",
		ProducesMediaTypes: []string{"application/json"},
		ConsumesMediaTypes: []string{"application/json"},
		Schemes:            []string{"http", "https"},
		Params:             params,
		Reader:             &AdminCreateUserReader{formats: a.formats},
		AuthInfo:           authInfo,
		Context:            params.Context,
		Client:             params.HTTPClient,
	})
	if err != nil {
		return nil, err
	}
	success, ok := result.(*AdminCreateUserCreated)
	if ok {
		return success, nil
	}
	// unexpected success response
	// safeguard: normally, absent a default response, unknown success responses return an error above: so this is a codegen issue
	msg := fmt.Sprintf("unexpected success response for adminCreateUser: API contract not enforced by server. Client expected to get an error, but got: %T", result)
	panic(msg)
}

/*
AdminDeleteUser deletes a user
*/
func (a *Client) AdminDeleteUser(params *AdminDeleteUserParams, authInfo runtime.ClientAuthInfoWriter) (*AdminDeleteUserNoContent, error) {
	// TODO: Validate the params before sending
	if params == nil {
		params = NewAdminDeleteUserParams()
	}

	result, err := a.transport.Submit(&runtime.ClientOperation{
		ID:                 "adminDeleteUser",
		Method:             "DELETE",
		PathPattern:        "/admin/users/{username}",
		ProducesMediaTypes: []string{"application/json"},
		ConsumesMediaTypes: []string{"application/json", "text/plain"},
		Schemes:            []string{"http", "https"},
		Params:             params,
		Reader:             &AdminDeleteUserReader{formats: a.formats},
		AuthInfo:           authInfo,
		Context:            params.Context,
		Client:             params.HTTPClient,
	})
	if err != nil {
		return nil, err
	}
	success, ok := result.(*AdminDeleteUserNoContent)
	if ok {
		return success, nil
	}
	// unexpected success response
	// safeguard: normally, absent a default response, unknown success responses return an error above: so this is a codegen issue
	msg := fmt.Sprintf("unexpected success response for adminDeleteUser: API contract not enforced by server. Client expected to get an error, but got: %T", result)
	panic(msg)
}

/*
AdminDeleteUserPublicKey deletes a user s public key
*/
func (a *Client) AdminDeleteUserPublicKey(params *AdminDeleteUserPublicKeyParams, authInfo runtime.ClientAuthInfoWriter) (*AdminDeleteUserPublicKeyNoContent, error) {
	// TODO: Validate the params before sending
	if params == nil {
		params = NewAdminDeleteUserPublicKeyParams()
	}

	result, err := a.transport.Submit(&runtime.ClientOperation{
		ID:                 "adminDeleteUserPublicKey",
		Method:             "DELETE",
		PathPattern:        "/admin/users/{username}/keys/{id}",
		ProducesMediaTypes: []string{"application/json"},
		ConsumesMediaTypes: []string{"application/json", "text/plain"},
		Schemes:            []string{"http", "https"},
		Params:             params,
		Reader:             &AdminDeleteUserPublicKeyReader{formats: a.formats},
		AuthInfo:           authInfo,
		Context:            params.Context,
		Client:             params.HTTPClient,
	})
	if err != nil {
		return nil, err
	}
	success, ok := result.(*AdminDeleteUserPublicKeyNoContent)
	if ok {
		return success, nil
	}
	// unexpected success response
	// safeguard: normally, absent a default response, unknown success responses return an error above: so this is a codegen issue
	msg := fmt.Sprintf("unexpected success response for adminDeleteUserPublicKey: API contract not enforced by server. Client expected to get an error, but got: %T", result)
	panic(msg)
}

/*
AdminEditUser edits an existing user
*/
func (a *Client) AdminEditUser(params *AdminEditUserParams, authInfo runtime.ClientAuthInfoWriter) (*AdminEditUserOK, error) {
	// TODO: Validate the params before sending
	if params == nil {
		params = NewAdminEditUserParams()
	}

	result, err := a.transport.Submit(&runtime.ClientOperation{
		ID:                 "adminEditUser",
		Method:             "PATCH",
		PathPattern:        "/admin/users/{username}",
		ProducesMediaTypes: []string{"application/json"},
		ConsumesMediaTypes: []string{"application/json"},
		Schemes:            []string{"http", "https"},
		Params:             params,
		Reader:             &AdminEditUserReader{formats: a.formats},
		AuthInfo:           authInfo,
		Context:            params.Context,
		Client:             params.HTTPClient,
	})
	if err != nil {
		return nil, err
	}
	success, ok := result.(*AdminEditUserOK)
	if ok {
		return success, nil
	}
	// unexpected success response
	// safeguard: normally, absent a default response, unknown success responses return an error above: so this is a codegen issue
	msg := fmt.Sprintf("unexpected success response for adminEditUser: API contract not enforced by server. Client expected to get an error, but got: %T", result)
	panic(msg)
}

/*
AdminGetAllOrgs lists all organizations
*/
func (a *Client) AdminGetAllOrgs(params *AdminGetAllOrgsParams, authInfo runtime.ClientAuthInfoWriter) (*AdminGetAllOrgsOK, error) {
	// TODO: Validate the params before sending
	if params == nil {
		params = NewAdminGetAllOrgsParams()
	}

	result, err := a.transport.Submit(&runtime.ClientOperation{
		ID:                 "adminGetAllOrgs",
		Method:             "GET",
		PathPattern:        "/admin/orgs",
		ProducesMediaTypes: []string{"application/json"},
		ConsumesMediaTypes: []string{"application/json", "text/plain"},
		Schemes:            []string{"http", "https"},
		Params:             params,
		Reader:             &AdminGetAllOrgsReader{formats: a.formats},
		AuthInfo:           authInfo,
		Context:            params.Context,
		Client:             params.HTTPClient,
	})
	if err != nil {
		return nil, err
	}
	success, ok := result.(*AdminGetAllOrgsOK)
	if ok {
		return success, nil
	}
	// unexpected success response
	// safeguard: normally, absent a default response, unknown success responses return an error above: so this is a codegen issue
	msg := fmt.Sprintf("unexpected success response for adminGetAllOrgs: API contract not enforced by server. Client expected to get an error, but got: %T", result)
	panic(msg)
}

/*
AdminGetAllUsers lists all users
*/
func (a *Client) AdminGetAllUsers(params *AdminGetAllUsersParams, authInfo runtime.ClientAuthInfoWriter) (*AdminGetAllUsersOK, error) {
	// TODO: Validate the params before sending
	if params == nil {
		params = NewAdminGetAllUsersParams()
	}

	result, err := a.transport.Submit(&runtime.ClientOperation{
		ID:                 "adminGetAllUsers",
		Method:             "GET",
		PathPattern:        "/admin/users",
		ProducesMediaTypes: []string{"application/json"},
		ConsumesMediaTypes: []string{"application/json", "text/plain"},
		Schemes:            []string{"http", "https"},
		Params:             params,
		Reader:             &AdminGetAllUsersReader{formats: a.formats},
		AuthInfo:           authInfo,
		Context:            params.Context,
		Client:             params.HTTPClient,
	})
	if err != nil {
		return nil, err
	}
	success, ok := result.(*AdminGetAllUsersOK)
	if ok {
		return success, nil
	}
	// unexpected success response
	// safeguard: normally, absent a default response, unknown success responses return an error above: so this is a codegen issue
	msg := fmt.Sprintf("unexpected success response for adminGetAllUsers: API contract not enforced by server. Client expected to get an error, but got: %T", result)
	panic(msg)
}

// SetTransport changes the transport on the client
func (a *Client) SetTransport(transport runtime.ClientTransport) {
	a.transport = transport
}
